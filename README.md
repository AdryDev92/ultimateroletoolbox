# Ultimate Role's Toolbox
Hosted page in GitHub with copyright. If you want collaborate with the project, please contact with the original author.
Free distribution isn't allowed.

---

## Featured patch notes ![status](https://img.shields.io/badge/status-WIP-orange.svg) ![version](https://img.shields.io/badge/alpha%20version-v0.2-green.svg?colorB=00C106) ![build](https://img.shields.io/badge/build-0201-green.svg?colorB=00C106)
In this version we're debugging the counters' programming, so is possible that some counters don't work.
The `mechanics.js` file only can be modified by the author, but if you have an idea to improve this or others files, you can do <code>fork</code> to your own repo, modify the relevant code and launch a <code>pull request</code>. If works good, the author will <code>merge</code> the new code.
### Next features ![next-patch](https://img.shields.io/badge/patch-v0.3—v0.5-green.svg?colorB=00C106)

- **Bag**. A new note zone where you will can write all items to your game.
- **Character sheet**. An app where you will can create your own character in a few minutes.
- **New background**. We are testing some backgrounds to give you a better experience when you use the app.



### Bugs
```
v0.1 - Fixed a little bug that caused that all dices show the same results.
```

## Other important details
You can participate in this project if you contact me, <a href="https://github.com/AdryDev92">AdryDev92</a>.
You only need follow me, and when I accept you, I will add you as collaborator. Thanks for reading.
